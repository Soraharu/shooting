var gulp = require("gulp");
var gulpServer = require("gulp-webserver");

gulp.task('webserver', function() {
    gulp.src("dev/").pipe(gulpServer({
        port: 8080,
        host: "127.0.0.1",
        livereload: true
    }));
});

gulp.task('libs', function() {
    return gulp.src([
            'node_modules/pixi.js/dist/pixi.min.js',
            'node_modules/pixi.js/dist/pixi.min.js.map',
            'node_modules/pixi-sound/dist/pixi-sound.js',
            'node_modules/pixi-sound/dist/pixi-sound.js.map',
            'node_modules/requirejs/require.js'
        ])
        .pipe(gulp.dest("dev/library"));
});

gulp.task('default', gulp.series('webserver'));