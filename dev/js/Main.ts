import { Application, Container, Graphics, Ticker} from "pixi.js";
import {} from "pixi-plugin-bump";

var applicaton:Application;

var htmlid = <HTMLCanvasElement>(document.getElementById("shoot"));

applicaton = new PIXI.Application({
    backgroundColor: 0xDEDEBE,
    view: htmlid
});

var timer:Ticker = PIXI.Ticker.shared;

//Our role
class Ship
{
    private space:Graphics = new PIXI.Graphics();
    private bullets:Graphics[]=[];
    private bulletarg:number[]=[];

    private lfrt:number = 0;
    private updown:number = 0;
    private ang:number = 0;

    public life:number = 100;

    //record keydown
    private map:{[key: string]: string} = {};

    //judge "move" keycode
    private isKeydown:boolean = false;

    //judge "shoot" keycode
    private isShoot:boolean = false;

    //shooting emy
    private Shootemy:boolean = false;

    private stage:Container;
    private shemy:Graphics = new PIXI.Graphics();

    private bump = new PIXI.extras.Bump();

    //keydown judge
    private keyDownFn = (event: KeyboardEvent) => {
        this.map[event.key] = 'Down';   
        if (event.key.toLowerCase() == "a" || 
            event.key.toLowerCase() == "d" ||
            event.key.toLowerCase() == "w" ||
            event.key.toLowerCase() == "s")
        {
            this.isKeydown = true;   
        }

        //move left or right
        if (this.map["d"] == 'Down' || this.map["a"] == 'Down')
        {
            if (this.map["d"] == 'Down' && this.map["a"] == 'Down')
            {
                this.lfrt = 0;
            }
            else
            {
                this.lfrt = (this.map["d"] == 'Down') ? 7 : -7;
            }   
        }
        

        //move up or down
        if (this.map["w"] == 'Down' || this.map["s"] == 'Down')
        {
            if (this.map["w"] == 'Down' && this.map["s"] == 'Down')
            {
                this.updown = 0;
            }
            else
            {
                this.updown = (this.map["s"] == 'Down') ? 7 : -7;
            } 
        }    
    }

    //keyup judge
    private keyUpFn = (event: KeyboardEvent) => {
        this.map[event.key] = 'Up';

        //move left or right
        if (this.map["d"] == 'Down' || this.map["a"] == 'Down')
        {
            this.lfrt = (this.map["d"] == 'Down') ? 5 : -5;           
        }
        //move up or down
        else if (this.map["w"] == 'Down' || this.map["s"] == 'Down')
        {
            this.updown = (this.map["s"] == 'Down') ? 5 : -5;
        }
        else
        {
            this.lfrt = 0;
            this.updown = 0;
        } 
    }

    private keyJud = () =>
    {
        //ship and map move
        this.space.x += this.lfrt;
        this.stage.pivot.x += this.lfrt;
        this.space.y += this.updown;
        this.stage.pivot.y += this.updown;
        if (this.bump.hitTestPoint(this.space, this.shemy))
        {
            this.space.x -= this.lfrt;
            this.stage.pivot.x -= this.lfrt;
            this.space.y -= this.updown;
            this.stage.pivot.y -= this.updown;
        }
    }

    //ship turn toward
    private msMoveFn = (event: MouseEvent) => {
        //ship center point
        let cntx = event.x - innerWidth / 2;
        let cnty = event.y - innerHeight / 2;

        if (cnty < 0 || cntx < 0)
        {
            //four
            if (cnty <= 0 && cntx >= 0)
             {
                this.ang = Math.atan2(Math.abs(cntx), Math.abs(cnty));
            }
            //two
            else if (cnty >= 0 && cntx <= 0)
            {
                this.ang = Math.atan2(Math.abs(cnty), Math.abs(cntx)) * -1 - Math.PI / 2;
            }
            //three
            else
            {
                this.ang = Math.atan2(Math.abs(cntx), Math.abs(cnty)) * -1;
            }
        }
        else
        {
            this.ang = Math.atan2(Math.abs(cnty), Math.abs(cntx)) + Math.PI / 2; 
        }
        this.space.rotation = this.ang;
    }

    private msClkFn = (event: MouseEvent) => {
        //left mouse button
        if (event.button == 0)
        {
            this.isShoot = true;
        }
    }

    private msEndFn = (event: MouseEvent) => {
        //left mouse button
        if (event.button == 0)
        {
            this.isShoot = false;
        }
    }

    private createbullet = () =>
    {
        if (!this.isShoot) return;
        let bullet:Graphics = new PIXI.Graphics();
        let colorMatrix = new PIXI.filters.ColorMatrixFilter();
        bullet.setTransform(this.space.x ,this.space.y);
        bullet.beginFill(0xFFFFFF);
        bullet.lineStyle(0x000000);
        bullet.moveTo(-10, -20);
        bullet.lineTo(10, -20);
        bullet.lineTo(10, 20);
        bullet.lineTo(-10, 20);
        bullet.endFill();
        
        bullet.rotation = this.ang;
        bullet.pivot.x = -5;
        bullet.pivot.y = 5;

        bullet.filters = [colorMatrix];
        colorMatrix.lsd(true);
        this.bullets.push(bullet);
        this.stage.addChildAt(bullet, 0);
        this.bulletarg.push(this.ang);
    }

    private shooting = () =>
    {
        for (let i = 0; i < this.bullets.length; i++)
        {
            this.bullets[i].x -= Math.sin(this.bulletarg[i]) * -50;
            this.bullets[i].y -= Math.cos(this.bulletarg[i]) * 50;
        }
    }

    public getSp = () =>
    {
        return this.space;
    }

    public shipShoot = (emysp: Graphics) =>
    {
        this.Shootemy = false;
        for (let i = 0; i < this.bullets.length; i++)
        {
            if (this.bump.hitTestPoint(this.bullets[i], emysp))
            {
               this.Shootemy = true;             
               this.stage.removeChild(this.bullets[i]);
               this.bullets.splice(i, 1);
               this.bulletarg.splice(i, 1);
            }
        } 
        //attacked Enemy's color
        if (this.Shootemy)
        {
            emysp.clear();
            emysp.beginFill(0xFFFFFF);
            emysp.arc(0, 0, 50, -0.5 * Math.PI, 1.5 * Math.PI);
            emysp.endFill();              
        }
        else
        {
            emysp.clear();
            emysp.beginFill(0x000000);
            emysp.arc(0, 0, 50, -0.5 * Math.PI, 1.5 * Math.PI);
            emysp.endFill();              
        }      
    }

    public injship()
    {
        //ship front
        this.space.beginFill(0xFFFFFF);
        this.space.moveTo(0, 2);
        this.space.lineTo(20, -16);
        this.space.lineTo(0, -58);
        this.space.lineTo(-20, -16);
        this.space.endFill();

        this.space.beginFill(0xFFFFFF);
        this.space.moveTo(1, 2);
        this.space.lineTo(1, 20);
        this.space.lineTo(20, -14);
        this.space.endFill();

        //ship behind
        this.space.beginFill(0xFFFFFF);
        this.space.moveTo(-1, 2);
        this.space.lineTo(-1, 20);
        this.space.lineTo(-20, -14);
        this.space.endFill();

        this.space.beginFill(0xFFFFFF);
        this.space.drawCircle(23, 20, 7);
        this.space.endFill();

        this.space.beginFill(0xFFFFFF);
        this.space.drawCircle(-23, 20, 7);
        this.space.endFill();

        //ship center
        this.space.beginFill(0xFFFFFF);
        this.space.drawCircle(0, -4, 10);
        this.space.endFill();
    }

    public orgship()
    {
        //ship front
        this.space.beginFill(0xB9B973);
        this.space.moveTo(0, 2);
        this.space.lineTo(20, -16);
        this.space.lineTo(0, -58);
        this.space.lineTo(-20, -16);
        this.space.endFill();

        this.space.beginFill(0xB9B973);
        this.space.moveTo(1, 2);
        this.space.lineTo(1, 20);
        this.space.lineTo(20, -14);
        this.space.endFill();

        //ship behind
        this.space.beginFill(0xB9B973);
        this.space.moveTo(-1, 2);
        this.space.lineTo(-1, 20);
        this.space.lineTo(-20, -14);
        this.space.endFill();

        this.space.beginFill(0xB9B973);
        this.space.drawCircle(23, 20, 7);
        this.space.endFill();

        this.space.beginFill(0xB9B973);
        this.space.drawCircle(-23, 20, 7);
        this.space.endFill();

        //ship center
        this.space.beginFill(0x000000);
        this.space.drawCircle(0, -4, 10);
        this.space.endFill();
    }
    
    constructor(stage:Container, emysp: Graphics)
    {
        this.stage = stage;
        this.shemy = emysp;


        //Fixed point
        this.space.setTransform(innerWidth / 2, innerHeight / 2);
        this.orgship();
        
        this.stage.addChild(this.space);
        this.space.pivot.x = 0;
        this.space.pivot.y = 0;
        //this.space.rotation = Math.PI * 90 /180;

        //ship move
        window.addEventListener(
            "keydown", this.keyDownFn
        );
        window.addEventListener(
            "keyup", this.keyUpFn
        );

        //ship target
        window.addEventListener(
            "mousemove", this.msMoveFn
        );

        //ship shooting
        window.addEventListener(
            "mousedown", this.msClkFn
        );
        window.addEventListener(
            "mouseup", this.msEndFn
        );

        timer.add((delta)=>
        {
           this.keyJud();
           this.shooting();
        });
        let bulletMaker = setInterval(() => {this.createbullet();}, 100);
    }
}

class Enemy
{
    private emy:Graphics = new PIXI.Graphics();
    private emystage:Container;
    private emy_bul:Graphics[]=[];
    private emy_arg:number[]=[];

    //shooting ship
    private Shootship:boolean = false;

    private bump = new PIXI.extras.Bump();

    public getSp()
    {
        return this.emy;
    }

    //Enemy's bullet
    public createbullet = (ship: Graphics) =>
    {
        let bullet:Graphics = new PIXI.Graphics();
        bullet.beginFill(0xFFA042, 0.4);
        bullet.lineStyle(0x000000);
        bullet.x = this.emy.x;
        bullet.y = this.emy.y;
        bullet.drawRect(0, 0, 20, 20);
        bullet.endFill();

        
        let ang = Math.atan2(ship.y - this.emy.y, ship.x - this.emy.x) + Math.PI / 2;
        bullet.rotation = ang;
        this.emystage.addChildAt(bullet, 0);
        this.emy_bul.push(bullet);     
        this.emy_arg.push(ang);
    }

    private shooting = () =>
    {
        for (let i = 0; i < this.emy_bul.length; i++)
        {
            this.emy_bul[i].x -= Math.sin(this.emy_arg[i]) * -10;
            this.emy_bul[i].y -= Math.cos(this.emy_arg[i]) * 10;
        }       
    }

    public shipShoot = (clasship :Ship) =>
    {
        let ship:Graphics = clasship.getSp();
        let ship_life = clasship.life;
        this.Shootship = false;

        for (let i = 0; i < this.emy_bul.length; i++)
        {
            if (this.bump.hitTestPoint(this.emy_bul[i], ship))
            {
               this.Shootship = true;             
               this.emystage.removeChild(this.emy_bul[i]);
               this.emy_bul.splice(i, 1);
               this.emy_arg.splice(i, 1);
            }
        }
         //attacked player's color
         if (this.Shootship)
         {
             ship.clear();
             clasship.injship();
             ship_life -= 10;            
         }
         else
         {
             ship.clear();  
             clasship.orgship();             
         }      
    }

    constructor(stage: Container)
    {
        this.emy.x = 50 + Math.floor(Math.random() * (innerWidth - 50));
        this.emy.y = 50 + Math.floor(Math.random() * (innerHeight - 50));
        this.emystage = stage;
        this.emy.beginFill(0x000000);
        this.emy.arc(0, 0, 50, -0.5 * Math.PI, 1.5 * Math.PI);
        this.emy.endFill();

        this.emystage.addChild(this.emy);
        timer.add(()=>{
            this.shooting();
        });
        setInterval(() => this.shooting(), 50);
    }
}


const playBtn = new PIXI.Text(
    "play",
    {
        fontSize: 40,
        fill: 0xFF00000
    },
);
playBtn.x = window.innerWidth / 2;
playBtn.y = window.innerHeight / 2;
playBtn.anchor.set(0.5);
playBtn.interactive = true;
playBtn.buttonMode = true;

applicaton.stage.addChild(playBtn);
applicaton.renderer.view.style.position = "absolute";
applicaton.renderer.view.style.display = "block";
applicaton.renderer.resize(window.innerWidth, window.innerHeight);

playBtn.on("mousedown", () => {
    applicaton.stage.removeChild(playBtn);
    document.body.style.cursor = "none";
    setTimeout(() => {
        setup();
    }, 300);
})



function setup()
{
    var galxy = new PIXI.Container();
    applicaton.stage.addChild(galxy);

    var Myemy  = new Enemy(galxy);
    var MyPlayer = new Ship(galxy, Myemy.getSp());
    
    //ship shoot
    timer.add(()=>{
        MyPlayer.shipShoot(Myemy.getSp());
        Myemy.shipShoot(MyPlayer);
    })

    //auto enemy shotting  
    setTimeout(() => {
        setInterval(() => Myemy.createbullet(MyPlayer.getSp()), 500);
    }, 1000);
}




