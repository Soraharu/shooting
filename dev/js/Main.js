define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var applicaton;
    var htmlid = (document.getElementById("shoot"));
    applicaton = new PIXI.Application({
        width: 1280,
        height: 720,
        backgroundColor: 0xDEDEBE,
        view: htmlid
    });
    var timer = PIXI.Ticker.shared;
    //Our role
    var Ship = /** @class */ (function () {
        function Ship(stage, emysp) {
            var _this = this;
            this.space = new PIXI.Graphics();
            this.bullets = [];
            this.bulletarg = [];
            this.lfrt = 0;
            this.updown = 0;
            this.ang = 0;
            this.life = 100;
            //record keydown
            this.map = {};
            //judge "move" keycode
            this.isKeydown = false;
            //judge "shoot" keycode
            this.isShoot = false;
            //shooting emy
            this.Shootemy = false;
            this.shemy = new PIXI.Graphics();
            this.bump = new PIXI.extras.Bump();
            //keydown judge
            this.keyDownFn = function (event) {
                _this.map[event.key] = 'Down';
                if (event.key.toLowerCase() == "a" ||
                    event.key.toLowerCase() == "d" ||
                    event.key.toLowerCase() == "w" ||
                    event.key.toLowerCase() == "s") {
                    _this.isKeydown = true;
                }
                //move left or right
                if (_this.map["d"] == 'Down' || _this.map["a"] == 'Down') {
                    if (_this.map["d"] == 'Down' && _this.map["a"] == 'Down') {
                        _this.lfrt = 0;
                    }
                    else {
                        _this.lfrt = (_this.map["d"] == 'Down') ? 7 : -7;
                    }
                }
                //move up or down
                if (_this.map["w"] == 'Down' || _this.map["s"] == 'Down') {
                    if (_this.map["w"] == 'Down' && _this.map["s"] == 'Down') {
                        _this.updown = 0;
                    }
                    else {
                        _this.updown = (_this.map["s"] == 'Down') ? 7 : -7;
                    }
                }
            };
            //keyup judge
            this.keyUpFn = function (event) {
                _this.map[event.key] = 'Up';
                //move left or right
                if (_this.map["d"] == 'Down' || _this.map["a"] == 'Down') {
                    _this.lfrt = (_this.map["d"] == 'Down') ? 5 : -5;
                }
                //move up or down
                else if (_this.map["w"] == 'Down' || _this.map["s"] == 'Down') {
                    _this.updown = (_this.map["s"] == 'Down') ? 5 : -5;
                }
                else {
                    _this.lfrt = 0;
                    _this.updown = 0;
                }
            };
            this.keyJud = function () {
                //ship and map move
                _this.space.x += _this.lfrt;
                _this.stage.pivot.x += _this.lfrt;
                _this.space.y += _this.updown;
                _this.stage.pivot.y += _this.updown;
                if (_this.bump.hitTestPoint(_this.space, _this.shemy)) {
                    _this.space.x -= _this.lfrt;
                    _this.stage.pivot.x -= _this.lfrt;
                    _this.space.y -= _this.updown;
                    _this.stage.pivot.y -= _this.updown;
                }
            };
            //ship turn toward
            this.msMoveFn = function (event) {
                //ship center point
                var cntx = event.x - innerWidth / 2;
                var cnty = event.y - innerHeight / 2;
                if (cnty < 0 || cntx < 0) {
                    //four
                    if (cnty <= 0 && cntx >= 0) {
                        _this.ang = Math.atan2(Math.abs(cntx), Math.abs(cnty));
                    }
                    //two
                    else if (cnty >= 0 && cntx <= 0) {
                        _this.ang = Math.atan2(Math.abs(cnty), Math.abs(cntx)) * -1 - Math.PI / 2;
                    }
                    //three
                    else {
                        _this.ang = Math.atan2(Math.abs(cntx), Math.abs(cnty)) * -1;
                    }
                }
                else {
                    _this.ang = Math.atan2(Math.abs(cnty), Math.abs(cntx)) + Math.PI / 2;
                }
                _this.space.rotation = _this.ang;
            };
            this.msClkFn = function (event) {
                //left mouse button
                if (event.button == 0) {
                    _this.isShoot = true;
                }
            };
            this.msEndFn = function (event) {
                //left mouse button
                if (event.button == 0) {
                    _this.isShoot = false;
                }
            };
            this.createbullet = function () {
                if (!_this.isShoot)
                    return;
                var bullet = new PIXI.Graphics();
                var colorMatrix = new PIXI.filters.ColorMatrixFilter();
                bullet.setTransform(_this.space.x, _this.space.y);
                bullet.beginFill(0xFFFFFF);
                bullet.lineStyle(0x000000);
                bullet.moveTo(-10, -20);
                bullet.lineTo(10, -20);
                bullet.lineTo(10, 20);
                bullet.lineTo(-10, 20);
                bullet.endFill();
                bullet.rotation = _this.ang;
                bullet.pivot.x = -5;
                bullet.pivot.y = 5;
                bullet.filters = [colorMatrix];
                colorMatrix.lsd(true);
                _this.bullets.push(bullet);
                _this.stage.addChildAt(bullet, 0);
                _this.bulletarg.push(_this.ang);
            };
            this.shooting = function () {
                for (var i = 0; i < _this.bullets.length; i++) {
                    _this.bullets[i].x -= Math.sin(_this.bulletarg[i]) * -50;
                    _this.bullets[i].y -= Math.cos(_this.bulletarg[i]) * 50;
                }
            };
            this.getSp = function () {
                return _this.space;
            };
            this.shipShoot = function (emysp) {
                _this.Shootemy = false;
                for (var i = 0; i < _this.bullets.length; i++) {
                    if (_this.bump.hitTestPoint(_this.bullets[i], emysp)) {
                        _this.Shootemy = true;
                        _this.stage.removeChild(_this.bullets[i]);
                        _this.bullets.splice(i, 1);
                        _this.bulletarg.splice(i, 1);
                    }
                }
                //attacked Enemy's color
                if (_this.Shootemy) {
                    emysp.clear();
                    emysp.beginFill(0xFFFFFF);
                    emysp.arc(0, 0, 50, -0.5 * Math.PI, 1.5 * Math.PI);
                    emysp.endFill();
                }
                else {
                    emysp.clear();
                    emysp.beginFill(0x000000);
                    emysp.arc(0, 0, 50, -0.5 * Math.PI, 1.5 * Math.PI);
                    emysp.endFill();
                }
            };
            this.stage = stage;
            this.shemy = emysp;
            //Fixed point
            this.space.setTransform(innerWidth / 2, innerHeight / 2);
            this.orgship();
            this.stage.addChild(this.space);
            this.space.pivot.x = 0;
            this.space.pivot.y = 0;
            //this.space.rotation = Math.PI * 90 /180;
            //ship move
            window.addEventListener("keydown", this.keyDownFn);
            window.addEventListener("keyup", this.keyUpFn);
            //ship target
            window.addEventListener("mousemove", this.msMoveFn);
            //ship shooting
            window.addEventListener("mousedown", this.msClkFn);
            window.addEventListener("mouseup", this.msEndFn);
            timer.add(function (delta) {
                _this.keyJud();
                _this.shooting();
            });
            var bulletMaker = setInterval(function () { _this.createbullet(); }, 100);
        }
        Ship.prototype.injship = function () {
            //ship front
            this.space.beginFill(0xFFFFFF);
            this.space.moveTo(0, 2);
            this.space.lineTo(20, -16);
            this.space.lineTo(0, -58);
            this.space.lineTo(-20, -16);
            this.space.endFill();
            this.space.beginFill(0xFFFFFF);
            this.space.moveTo(1, 2);
            this.space.lineTo(1, 20);
            this.space.lineTo(20, -14);
            this.space.endFill();
            //ship behind
            this.space.beginFill(0xFFFFFF);
            this.space.moveTo(-1, 2);
            this.space.lineTo(-1, 20);
            this.space.lineTo(-20, -14);
            this.space.endFill();
            this.space.beginFill(0xFFFFFF);
            this.space.drawCircle(23, 20, 7);
            this.space.endFill();
            this.space.beginFill(0xFFFFFF);
            this.space.drawCircle(-23, 20, 7);
            this.space.endFill();
            //ship center
            this.space.beginFill(0xFFFFFF);
            this.space.drawCircle(0, -4, 10);
            this.space.endFill();
        };
        Ship.prototype.orgship = function () {
            //ship front
            this.space.beginFill(0xB9B973);
            this.space.moveTo(0, 2);
            this.space.lineTo(20, -16);
            this.space.lineTo(0, -58);
            this.space.lineTo(-20, -16);
            this.space.endFill();
            this.space.beginFill(0xB9B973);
            this.space.moveTo(1, 2);
            this.space.lineTo(1, 20);
            this.space.lineTo(20, -14);
            this.space.endFill();
            //ship behind
            this.space.beginFill(0xB9B973);
            this.space.moveTo(-1, 2);
            this.space.lineTo(-1, 20);
            this.space.lineTo(-20, -14);
            this.space.endFill();
            this.space.beginFill(0xB9B973);
            this.space.drawCircle(23, 20, 7);
            this.space.endFill();
            this.space.beginFill(0xB9B973);
            this.space.drawCircle(-23, 20, 7);
            this.space.endFill();
            //ship center
            this.space.beginFill(0x000000);
            this.space.drawCircle(0, -4, 10);
            this.space.endFill();
        };
        return Ship;
    }());
    var Enemy = /** @class */ (function () {
        function Enemy(stage) {
            var _this = this;
            this.emy = new PIXI.Graphics();
            this.emy_bul = [];
            this.emy_arg = [];
            //shooting ship
            this.Shootship = false;
            this.bump = new PIXI.extras.Bump();
            //Enemy's bullet
            this.createbullet = function (ship) {
                var bullet = new PIXI.Graphics();
                bullet.beginFill(0xFFA042, 0.4);
                bullet.lineStyle(0x000000);
                bullet.x = _this.emy.x;
                bullet.y = _this.emy.y;
                bullet.drawRect(0, 0, 20, 20);
                bullet.endFill();
                var ang = Math.atan2(ship.y - _this.emy.y, ship.x - _this.emy.x) + Math.PI / 2;
                bullet.rotation = ang;
                _this.emystage.addChildAt(bullet, 0);
                _this.emy_bul.push(bullet);
                _this.emy_arg.push(ang);
            };
            this.shooting = function () {
                for (var i = 0; i < _this.emy_bul.length; i++) {
                    _this.emy_bul[i].x -= Math.sin(_this.emy_arg[i]) * -10;
                    _this.emy_bul[i].y -= Math.cos(_this.emy_arg[i]) * 10;
                }
            };
            this.shipShoot = function (clasship) {
                var ship = clasship.getSp();
                var ship_life = clasship.life;
                _this.Shootship = false;
                for (var i = 0; i < _this.emy_bul.length; i++) {
                    if (_this.bump.hitTestPoint(_this.emy_bul[i], ship)) {
                        _this.Shootship = true;
                        _this.emystage.removeChild(_this.emy_bul[i]);
                        _this.emy_bul.splice(i, 1);
                        _this.emy_arg.splice(i, 1);
                    }
                }
                //attacked player's color
                if (_this.Shootship) {
                    ship.clear();
                    clasship.injship();
                    ship_life -= 10;
                }
                else {
                    ship.clear();
                    clasship.orgship();
                }
            };
            this.emy.x = 50 + Math.floor(Math.random() * (innerWidth - 50));
            this.emy.y = 50 + Math.floor(Math.random() * (innerHeight - 50));
            this.emystage = stage;
            this.emy.beginFill(0x000000);
            this.emy.arc(0, 0, 50, -0.5 * Math.PI, 1.5 * Math.PI);
            this.emy.endFill();
            this.emystage.addChild(this.emy);
            timer.add(function () {
                _this.shooting();
            });
            setInterval(function () { return _this.shooting(); }, 50);
        }
        Enemy.prototype.getSp = function () {
            return this.emy;
        };
        return Enemy;
    }());
    var playBtn = new PIXI.Text("play", {
        fontSize: 40,
        fill: 0xFF00000
    });
    playBtn.x = window.innerWidth / 2;
    playBtn.y = window.innerHeight / 2;
    playBtn.anchor.set(0.5);
    playBtn.interactive = true;
    playBtn.buttonMode = true;
    applicaton.stage.addChild(playBtn);
    applicaton.renderer.view.style.position = "absolute";
    applicaton.renderer.view.style.display = "block";
    applicaton.renderer.resize(window.innerWidth, window.innerHeight);
    playBtn.on("mousedown", function () {
        applicaton.stage.removeChild(playBtn);
        document.body.style.cursor = "none";
        setTimeout(function () {
            setup();
        }, 300);
    });
    function setup() {
        var galxy = new PIXI.Container();
        applicaton.stage.addChild(galxy);
        var Myemy = new Enemy(galxy);
        var MyPlayer = new Ship(galxy, Myemy.getSp());
        //ship shoot
        timer.add(function () {
            MyPlayer.shipShoot(Myemy.getSp());
            Myemy.shipShoot(MyPlayer);
        });
        //auto enemy shotting  
        setTimeout(function () {
            setInterval(function () { return Myemy.createbullet(MyPlayer.getSp()); }, 500);
        }, 1000);
    }
});
