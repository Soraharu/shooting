# 宇宙飛船
作品網址：https://soraharu.gitlab.io/shooting/

遊戲參考「尼爾：自動人形」中的駭客射擊小遊戲<br>
使用**typescript+pixijs**製作<br>

### 遊玩方式
1.船體會根據滑鼠游標方向進行旋轉，船頭指向游標的方向<br>
(為操作不受干擾，遊戲開始會將滑鼠游標隱藏)<br>
2.按下右鍵發射飛彈<br>
3.操作鍵w(上)、a(左)、s(下)、d(右)，移動船隻，避開敵方(圓球)攻擊<br>

目前遊戲為試射版本，可透過此程式練習相關遊戲的技巧，故目前沒有血量等遊戲機制
